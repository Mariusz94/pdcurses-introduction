#include <stdlib.h>
#include <math.h>
#include <curses.h>

/*
 * Define glyphs for drawing patterns:                      \ XN | XM /
 *     glyphsN  - normal form                            N   \  |  /   M
 *     glyphsM  - mirrored vertically or horizontally ----------|----------
 *     glyphsXN - reflected through oblique axis         M   /  |  \   N
 *     glyphsXM - oblique reflected and mirrored           / XM | XN \
 */
const char glyphsN[]  = { '|', '-', '/', '\\' };
const char glyphsM[]  = { '|', '-', '\\', '/' };
const char glyphsXN[] = { '-', '|', '/', '\\' };
const char glyphsXM[] = { '-', '|', '\\', '/' };

/*
 * Define line directions as indices to glyphs tables
 */
enum {
    LINE_N  = 0,    LINE_S  = 0,    // glyphsN[0]
    LINE_E  = 1,    LINE_W  = 1,    // glyphsN[1]
    LINE_NE = 2,    LINE_SW = 2,    // glyphsN[2]
    LINE_NW = 3,    LINE_SE = 3     // glyphsN[3]
};

/*
 * Gather coordinate increments and line directions for eight possible moves,
 * in clockwise order starting from North:
 *     movesDeltaX  - horizontal component of move vector
 *     movesDeltaY  - vertical component of move vector
 *     movesLineDir - line direction corresponding to move vector
 */
const int movesDeltaX[] = {  0, +1, +1, +1,  0, -1, -1, -1 };
const int movesDeltaY[] = { -1, -1,  0, +1, +1, +1,  0, -1 };
const int movesLineDir[] = {
    LINE_N, LINE_NE, LINE_E, LINE_SE, LINE_S, LINE_SW, LINE_W, LINE_NW
};

/*
 * Define colors used as foreground character attributes
 * (Black is repeated twice - these colors will be cycled periodically
 * and black color will erase characters printed on the screen)
 */
const short colors[] = {
    COLOR_RED, COLOR_YELLOW, COLOR_GREEN, COLOR_BLACK,
    COLOR_CYAN, COLOR_BLUE, COLOR_MAGENTA, COLOR_BLACK
};

/*
 * Define some useful constant symbols
 */
enum {
    MOVESNUM = 8,                   // Number of possible moves
    COLORSNUM = 8,                  // Number of elements in color table
    SYMMETRIES = 8,                 // Number of symmetric reflections
    RADIUS = 16,                    // Half the height of the display
    RADIUS_H = 2*RADIUS,            // Half the width of the display
    DIAMETER = 2*RADIUS,            // Height of the display
    DIAMETER_H = 2*DIAMETER,        // Width of the display
    BOTTOM = DIAMETER - 1,          // Coordinate of the bottom row
    RIGHT = DIAMETER_H - 1,         // Coordinate of the rightmost column
    COLOR_PERIOD = 10,              // Number of steps between changing color
    DELAY = 10                      // Delay period in milliseconds
};

/**
 * @brief Generate 8 symmetric reflections of a glyph at given location.
 * @param[in]  x,y         Coordinates of source point
 * @param[in]  g           Index of glyph in glyphsN array
 * @param[out] xArr, yArr  Arrays receiving all reflected coordinates
 * @param[out] glyphArr    Array receiving characters of reflected glyphs
 */
void populateSymmetries(int x, int y, int g,
                        int xArr[SYMMETRIES], int yArr[SYMMETRIES],
                        char glyphArr[SYMMETRIES]) {
    // Restrict the source point to one quarter of screen
    y %= RADIUS;
    x %= RADIUS_H;
    // Generate horizontal and vertical reflections
    xArr[0] = x;            yArr[0] = y;             glyphArr[0] = glyphsN[g];
    xArr[1] = x;            yArr[1] = BOTTOM - y;    glyphArr[1] = glyphsM[g];
    xArr[2] = RIGHT - x;    yArr[2] = y;             glyphArr[2] = glyphsM[g];
    xArr[3] = RIGHT - x;    yArr[3] = BOTTOM - y;    glyphArr[3] = glyphsN[g];
    // Reflect the source point through oblique axis
    int xs = 2*y + x%2;     // Ensure each point has unique reflection
    int ys = x/2;
    // Generate remaining four reflections
    xArr[4] = xs;           yArr[4] = ys;            glyphArr[4] = glyphsXN[g];
    xArr[5] = xs;           yArr[5] = BOTTOM - ys;   glyphArr[5] = glyphsXM[g];
    xArr[6] = RIGHT - xs;   yArr[6] = ys;            glyphArr[6] = glyphsXM[g];
    xArr[7] = RIGHT - xs;   yArr[7] = BOTTOM - ys;   glyphArr[7] = glyphsXN[g];
}

/**
 * @brief Add an increment to a value and ensure it is in specified interval
 * @param value   The value to be modified
 * @param delta   The increment to be added (can be negative)
 * @param period  The length of wrapping interval
 * @return The sum value+delta wrapped to the interval [0, period-1].
 */
unsigned addAndWrap(unsigned value, int delta, unsigned period) {
    int result = (int)value + delta;
    if (result < 0) {
        // Make the result positive preserving Euclidean division remainder
        result += period * (-result/period + 1);
    }
    return result % period;
}

int main() {
    // Start PDCurses
    initscr();
    start_color();
    curs_set(0);
    resize_term(DIAMETER, DIAMETER_H);  // Resize terminal to (roughly) square
    nodelay(stdscr, TRUE);        // Don't wait for user when reading keyboard
    refresh();

    // Initialize color pairs and prepare attributes for color output
    chtype attributes[COLORSNUM];
    int p;
    for (p = 0; p < COLORSNUM; p++) {
        init_pair(p+1,              // Color pairs are numbered from 1
                  colors[p],        // Foreground color
                  COLOR_BLACK);     // Background color
        // Combine all colors with A_BOLD attribute except COLOR_BLACK
        // (A_BOLD makes all colors brighter and black becomes gray)
        attributes[p] = COLOR_PAIR(p+1) |
                        (colors[p]!=COLOR_BLACK ? A_BOLD : A_NORMAL);
    }

    // The main loop
    int xPos = RADIUS_H / 2;        // Current position
    int yPos = RADIUS / 2;
    unsigned steps = 0;             // Counter of loop iterations
    unsigned move = 0;              // Current direction of movement
    int running = TRUE;             // Boolean flag for loop control
    while (running) {
        // Randomly change move direction and calculate new position
        move = addAndWrap(move, rand()%3 - 1, MOVESNUM);
        xPos = addAndWrap(xPos, movesDeltaX[move], RADIUS_H);
        yPos = addAndWrap(yPos, movesDeltaY[move], RADIUS);

        // Prepare 8 symmetric reflections
        int rows[SYMMETRIES];
        int cols[SYMMETRIES];
        char glyphs[SYMMETRIES];
        populateSymmetries(xPos, yPos, movesLineDir[move], cols, rows, glyphs);

        // Print all reflected copies
        int i;
        chtype attribute = attributes[steps/COLOR_PERIOD % COLORSNUM];
        for (i = 0; i < SYMMETRIES; i++) {
            mvaddch(rows[i], cols[i], glyphs[i] | attribute);
        }
        steps++;

        // Sleep and check keyboard
        napms(DELAY);
        int key = getch();
        switch (key) {
            case 033:            // ESCAPE key pressed
                running = FALSE;    // Finish loop at next iteration
                break;
            case 040:            // Space key pressed
                clear();            // Clear entire window
                break;
            default:             // Some other key pressed
            case ERR:            // or no key pressed at all
                continue;
            /* TODO: Add a pausing feature. If the user presses P key,
             * the screen freezes and no new patterns are drawn until
             * the next P key press.
             */
        }
    } // End of main loop

    // Finish PDCurses session
    endwin();
    return EXIT_SUCCESS;
}
