#include <stdlib.h>
#include <curses.h>

// Attributes used for drawing interface elements
const chtype ATTR_NORM  = COLOR_PAIR(1) | A_NORMAL;
const chtype ATTR_FOCUS = COLOR_PAIR(2) | A_BOLD;
const chtype ATTR_OFF   = COLOR_PAIR(3) | A_NORMAL;
const chtype ATTR_ON    = COLOR_PAIR(4) | A_BOLD;

// Useful constants
enum {
    ORDER = 5,
    BOX_WIDTH = 8,
    BOX_HEIGHT = 5
};

/* TODO: Get rid of global variables and introduce some method of
 * passing these variables between functions.
 */
int lightStates[ORDER][ORDER];  // On (1) or off (0) states of ligts
WINDOW* lights[ORDER][ORDER];   // Windows for displaying each light
int cursorRow = ORDER / 2;      // Cursor row (initially at center)
int cursorCol = ORDER / 2;      // Cursor column (initially at center)

/**
 * @brief Create ORDER x ORDER windows for each light on the board
 * @return TRUE on success, FALSE otherwise
 */
int createWindows() {
    int row;
    int col;
    for (row = 0; row < ORDER; row++) {
        for (col = 0; col < ORDER; col++) {
            lights[row][col] = newwin(BOX_HEIGHT, BOX_WIDTH,
                                      row*BOX_HEIGHT, col*BOX_WIDTH);
            if (!lights[row][col]) {
                return FALSE;
            }
        }
    }
    return TRUE;
}

/**
 * @brief Draw all light windows on the screen according to their on/off
 *        status and cursor position
 */
void refreshAll() {
    int row, col;
    for (row = 0; row < ORDER; row++) {
        for (col = 0; col < ORDER; col++) {
            if (row == cursorRow && col == cursorCol) {
                wattrset(lights[row][col], ATTR_FOCUS);
            } else {
                wattrset(lights[row][col], ATTR_NORM);
            }
            box(lights[row][col], 0, 0);
            int x, y;
            for (y = 1; y < BOX_HEIGHT-1; y++) {
                for (x = 1; x < BOX_WIDTH-1; x++) {
                    chtype fill = ACS_BLOCK |
                                  (lightStates[row][col] ? ATTR_ON : ATTR_OFF);
                    mvwaddch(lights[row][col], y, x, fill);
                }
            }
            // Draw the window on internal buffer (PDCurses virtual screen)
            wnoutrefresh(lights[row][col]);
        }
    }
    // Output internal buffer to physical terminal screen once for all windows
    doupdate();
}

int main() {
    initscr();
    start_color();
    curs_set(0);
    resize_term(ORDER*BOX_HEIGHT, ORDER*BOX_WIDTH);
    keypad(stdscr, TRUE);   // Turn on processing of non-alphanumeric keys
    refresh();

    init_pair(1, COLOR_BLUE, COLOR_BLACK);      // Normal border
    init_pair(2, COLOR_YELLOW, COLOR_BLACK);    // Cursor border
    init_pair(3, COLOR_BLACK, COLOR_BLACK);     // Light off
    init_pair(4, COLOR_WHITE, COLOR_BLACK);     // Light on

    if (!createWindows()) {
        endwin();
        return EXIT_FAILURE;
    }
    /* TODO: Add initialization of lights state.
     * At the beginning some ligts must be turned on according to
     * predefined or random pattern.
     */
    refreshAll();

    int running = TRUE;
    while (running) {
        int key = getch();
        switch (key) {
            case 033:   // ESCAPE
                running = FALSE; break;
            case 040:   // Space
                /* TODO: Add the original logic to light switching behavior.
                 * Toggling a light should also toggle all of its neighbors.
                 */
                lightStates[cursorRow][cursorCol] ^= 1; break;
            case KEY_DOWN:
                cursorRow = (cursorRow+1) % ORDER; break;
            case KEY_UP:
                cursorRow = (cursorRow-1+ORDER) % ORDER; break;
            case KEY_RIGHT:
                cursorCol = (cursorCol+1) % ORDER; break;
            case KEY_LEFT:
                cursorCol = (cursorCol-1+ORDER) % ORDER; break;
        }
        refreshAll();
        /* TODO: Add solution detection.
         * The game should end automatically when all lights become off.
         */
    }

    endwin();
    return EXIT_SUCCESS;
}
